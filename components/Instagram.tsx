type Props = {};

export default function Instagram({}: Props) {
  return (
    <div className="w-full h-64 flex place-content-center items-center my-6 bg-cover" style="background: url(https://via.placeholder.com/1080.png)" >
      <button className=" text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
        Instragram
      </button>
    </div>
  );
}
