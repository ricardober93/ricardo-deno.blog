import { useState } from "preact/hooks";
export default function Footer() {
  const [year, _] = useState(new Date().getFullYear());
  return (
    <footer className="w-full text-center h-16  flex justify-center items-center">
      © <span className="font-bold ml-1">{year}</span>, Made ❤️ With By{" "}
      <span className="font-bold ml-2">Ricardo Bermudez</span>
    </footer>
  );
}
