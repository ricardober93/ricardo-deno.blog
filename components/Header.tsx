import ButtonSuscribe from "../islands/ButtonSuscribe.tsx";

export function Header() {
    return (
      <header
        class="p-10 bg-yellow-400"
      >
        <section class="w-full flex justify-between items-center flex-col md:flex-row gap-5">
            <h1 class="font-bold text-4xl">Ricardo</h1>
            <ul class="flex justify-center items-center gap-4 flex-col md:flex-row">
                <li>All</li>
                <li>Frontend</li>
                <li>Backend</li>
                <li>Javascript</li>
                <li>tools</li>
            </ul>
            <ButtonSuscribe/>
        </section>
        <section class="mt-24  w-full h-44 flex flex-col place-content-center place-items-center" >
            <h6 class="font-bold mb-10" > Mi Blog </h6>
            <h2 class="font-bold text-6xl text-center lg:text-right" >Ricardo Bermudez</h2>
        </section>
        </header>
    );
  }