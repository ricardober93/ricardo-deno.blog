type Props = {
    title:string;

};

export default function ItemCard({}: Props) {
  return (
    <div class="mx-auto bg-white cursor-pointer dark:bg-gray-800 dark:border-gray-700">
      <section class="flex flex-row gap-6">
          <img
            class="w-full bg-cover max-h-32 w-32 rounded-t-lg"
            src="https://via.placeholder.com/150.png"
            alt=""
          />
          <div className="flex flex-col justify-between">
            <h5 class="text-xl font-bold tracking-tight text-gray-900 dark:text-white">
              Noteworthy technology acquisitions 2021
            </h5>
            <span class="text-sm text-gray-300">by autor</span>
          </div>
      </section>
    </div>
  );
}
