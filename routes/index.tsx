import { Head } from "$fresh/runtime.ts";
import { CardBlog } from "../components/Card.tsx";
import Footer from "../components/Footer.tsx";
import { Header } from "../components/Header.tsx";
import Instagram from "../components/Instagram.tsx";
import ItemCard from "../components/ItemCard.tsx";

export default function Home() {
  return (
    <>
      <Head>
        <title>Blog Ricardo</title>
      </Head>
      <main class="min-h-screen">
        <Header />
        <section class="container mx-auto my-6">
        <section class="w-full grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 auto-cols-max sm:auto-cols-min gap-5 p-6">
          {[0, 0, 0].map((card) => <CardBlog url="#" />)}
        </section>
        </section>
        <section class="container mx-auto my-6">
          <div class="w-full grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 auto-cols-max sm:auto-cols-min gap-8 p-6">
            {
              [0,1,2,3,4,5,6,7,8].map((item) => (
                <ItemCard title="" key={item} />
              ))
            }
          </div>
        </section>
        <Instagram />
        <Footer />
      </main>
    </>
  );
}
